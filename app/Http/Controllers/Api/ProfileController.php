<?php

namespace App\Http\Controllers\Api;

use App\Models\Profile;
use App\Models\Click;
use App\Models\Geo;
use App\Models\Subscription;
use App\Models\ProfileSubscription;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;

class ProfileController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Save Profile info to DB
     *@param \stdClass $profileObject
     * @return bool
     */
    public function create(\stdClass $profileObject)
    {
        ini_set('max_execution_time', '300');
//        DB::disableQueryLog();

        if($profileObject->profile_id):
        DB::beginTransaction();
        try {

            //save profile
            $profileDB = Profile::find($profileObject->profile_id);
            if(!$profileDB) {
                $profile = new Profile();
            }else{
                $profile = $profileDB;
            }

            //debug
            if(empty($profile)){echo "empty profile object for - " . $profileObject->profile_id; }

            $profile->profile_id = $profileObject->profile_id;
            $profile->email = $profileObject->email;
            $profile->import_from = $profileObject->import_from;
            $profile->save();

            unset($profile);


            //save clicks
            $profileClicks = $profileObject->clicks;
            if(!empty($profileClicks)) {

                //clean data before
                Click::where('profile_id', $profileObject->profile_id)->delete();

                foreach ($profileClicks as $profileClickKey => $profileClickValue) {
                    $click = new Click();

                    //debug
                    if(empty($click)){echo "empty click object for - " . $profileObject->profile_id; }

                    $click->profile_id = $profileObject->profile_id;
                    $click->campaign_id = $profileClickValue->campaign_id;
                    $click->url = $profileClickValue->url;
                    $click->time = json_encode($profileClickValue->time);
                    $click->save();

                    unset($click);
                }
            }

            //save geo
            $profileGeo = $profileObject->custom_vars->geo;
            if($profileGeo) {
                if (!Geo::find($profileObject->profile_id)) {
                    $geo = new Geo();
                } else {
                    $geo = Geo::find($profileObject->profile_id);
                }

                //debug
                if(empty($geo)){echo "empty geo object for - " . $profileObject->profile_id; }

                $geo->profile_id = $profileObject->profile_id;
                $geo->name = $profileGeo->name;
                $geo->view_count = $profileGeo->view_count;
                $geo->states = json_encode($profileGeo->states);
                $geo->save();

                unset($geo);
            }

            //save subscription and connect to profile
            $profileSubscriptions = $profileObject->custom_vars->current_subscriptions;
            if($profileSubscriptions) {
                //clean data before
                ProfileSubscription::where('profile_id', $profileObject->profile_id)->delete();

                foreach ($profileSubscriptions as $profileSubscriptionKey => $profileSubscriptionValue) {
                    //save subscription
                    if (!Subscription::find($profileSubscriptionValue->id)) {
                        $subscription = new Subscription();
                    } else {
                        $subscription = Subscription::find($profileSubscriptionValue->id);
                    }
//                $subscription = new Subscription();

                    //debug
                    if(empty($subscription)){echo "empty subscription object for - " . $profileObject->profile_id; }

                    $subscription->id = $profileSubscriptionValue->id;
                    $subscription->name = $profileSubscriptionValue->name;
                    $subscription->save();

                    //save relationship profile with subscription
                    $profileToSubscription = new ProfileSubscription();

                    //debug
                    if(empty($profileToSubscription)){echo "empty profile_sub object for - " . $profileObject->profile_id; }

                    $profileToSubscription->profile_id = $profileObject->profile_id;
                    $profileToSubscription->subscription_id = $profileSubscriptionValue->id;
                    $profileToSubscription->time = json_encode($profileSubscriptionValue->time);
                    $profileToSubscription->save();

                    unset($subscription);
                    unset($profileToSubscription);
                }
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();

            //debug code
            echo "<pre>---error----";
            print_r($e->getMessage());
            echo "---";
            print_r($profileObject);
            echo "</pre>";
            die('stop!');

            $this->returnJsonErrorResponse($e);
        }
        endif;

    }

    /**
     * Get information from DB for files
     *
     * @param array $criteria
     * @return array
     */
    public static function filesStatistics(array $criteria=[]){
        $profiles = DB::table('profiles')
            ->select('import_from', DB::raw('MAX(updated_at) as import_date'), DB::raw('count(profile_id) as file_profiles_count'))
            ->groupBy( 'import_from')
            ->get('file_profiles_count', 'import_from', 'import_date')->all();

        return $profiles;
    }

    /**
     * Get subscriptions from DB with counting subscribers
     *
     * @return array
     */
    public static function subscriptionsStatistics(){
        $subscriptions = Subscription::withCount('profiles')->get()->toArray();
        return $subscriptions;
    }

}
