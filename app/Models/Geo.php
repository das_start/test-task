<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Geo extends Model
{
    public $table = 'geos';
    protected  $primaryKey = 'profile_id';
}
