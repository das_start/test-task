<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileSubscription extends Model
{
    public $table = 'profile_subscriptions';
}
