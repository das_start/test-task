<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public $table = 'subscriptions';

    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile', 'profile_subscriptions', 'subscription_id', 'profile_id');
    }
}
