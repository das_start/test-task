<?php declare(strict_types=1);

namespace App\Modules\RemoteFilesImporter;

use App\Modules\Interfaces\IRemoteStorageService;
use App\Modules\RemoteFilesImporter\Exceptions\ValidationException;

use App\Http\Controllers\Api\ProfileController;

final class RemoteFilesImporterService
{
    private $remoteStorageService;
    private $repository;
    private $profile;

    public function __construct(IRemoteStorageService $remoteStorageService, RemoteFileImporterRepository $repository, ProfileController $profile)
    {
        $this->remoteStorageService = $remoteStorageService;
        $this->repository = $repository;
        $this->profile = $profile;
    }
    
    /**
     * @return array
     */
    public function getFilesAvailableForImport(): array {
        $files = $this->remoteStorageService->getRootDirectoryFiles();
        
        return array_filter($files, function(string $fileKey){
            return preg_match('#.json$#', $fileKey);
        });
    }
    
    /**
     * @param string $key
     * @return int
     * @throws ValidationException
     */
    public function importByKey(string $key): int {

        $filePath = $this->remoteStorageService->downloadRemoteFileByKey($key);
        if (!file_exists($filePath)) {
            throw new ValidationException('Can\'t download file for import');

        }
        try {

            if(filesize($filePath) > 100000000) {//205725277 - max now
//                $fileContent = $this->readBigFiles($filePath);
                $fullContentFromBigFile = $this->readBigFiles($filePath);
                $fullContentFromBigFileArray = [];
                foreach ($fullContentFromBigFile as $keyRow => $fileRow){
                    $fullContentFromBigFileArray[] = $fileRow;
                }

                if(!empty($fullContentFromBigFileArray))
                    $fileContent = $fullContentFromBigFileArray;
                else
                    throw new ValidationException('Can\'t parse big file for import');

            }else{
                $fileContent = file($filePath);
            }

            $count = 0;
            foreach ($fileContent as $row) {
                $jsonData = json_decode($row);

                if (!empty($jsonData)) {
                    /*$id = $jsonData->profile_id;
                    if ($this->repository->insertProfile($id)) {
                        $count++;
                    }*/

                    //save to DB
                    $jsonData->import_from = $key;
                    $this->profile->create($jsonData);
                    $count++;
                }
            }
        }catch (\Exception $e) {

            //debug code
            echo "<pre>---error----";
            print_r($e->getMessage());
            echo "</pre>";
            die('stop!');
        }

        return $count;
    }

    /**
     * @param string $filePath
     * @return object
     * @throws ValidationException
     */
    private function readBigFiles(string $filePath){
//    private function readBigFiles(string $filePath){
        $count = 0;
        $handle = fopen($filePath, "r");
        $fileContentArray = [];
        if ($handle) {
            while (!feof($handle)) {
                if(!empty(fgets($handle))) {
                    $line = trim(fgets($handle));

//                $fileContentArray[$count] = $line;

                    yield $line;

                    $count++;
                }
            }

            fclose($handle);
        } else {
            throw new ValidationException('Can\'t parse file for import');
        }

//        return $fileContentArray;
    }

}
